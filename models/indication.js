const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModel = require('./base');

const Indication = BaseModel({
    outlet_temperature: {
        type: Number,
        required: true
    },
    room_temperature: {
        type: Number,
        required: true
    },
    // 0 - низкое, 1 - норма, 2 - высокое
    water_pressure: {
        type: Number,
        required: true,
        enum: [0, 1, 2]
    },
    gas_ch4: {
        type: Boolean,
        required: true
    },
    gas_co: {
        type: Boolean,
        required: true
    },
    power: {
        type: Boolean,
        required: true
    },
    room_thermostat: {
        type: Boolean,
        required: true
    },
    room_thermostat_setting: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Indication', Indication);
