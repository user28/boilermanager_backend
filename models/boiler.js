const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModel = require('./base');

const Boiler = BaseModel({
    sensor_id: {
      type: String,
      required: true
    },
    address: {
      type: String,
      required: true
    }
});

module.exports = mongoose.model('Boiler', Boiler);