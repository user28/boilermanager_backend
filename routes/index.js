const express = require('express');
const moongose = require('mongoose');

const Indication = require('../models/indication');

const router = express.Router();

router.get('/', async (req, res) => {
  return res.json({success: true})
});

router.post('/indication', async (req, res) => {
  const { boiler, outlet_temperature, room_temperature, water_pressure, gas_ch4, gas_co, power, room_thermostat, room_thermostat_setting } = req.query;

  const indication = new Indication({
    _id: new moongose.Types.ObjectId(),
    outlet_temperature,
    room_temperature,
    water_pressure,
    gas_ch4,
    gas_co,
    power,
    room_thermostat,
    room_thermostat_setting
  });

  indication.save().then(result => {
    return res.json({success: true})
  }).catch(error => {
    console.log(error);
    return res.status(500).json({success: false})
  });
});

router.get('/indication', async (req, res) => {
  const indications = await Indication.find();

  return res.json({success: true, indications});
});

module.exports = router;
